#MattressBankApplication--Mattress 银行

模拟网上银行应用程序，使用XML文件作为数据源，通过GridView加载并显示银行交易数据。此外，通过Chart可视化的显示用户行为。


通过以上内容，展示如何在应用程序中通过图表和表格配合满足业务需求。 

![](http://i.imgur.com/ZZMkhuI.png)


![葡萄城控件微信服务号](http://www.gcpowertools.com.cn/newimages/qrgrapecity.png "葡萄城控件微信服务号")


------------------------
### 对应控件(ComponentOne Studio for ASP.NET Wijmo )： ###
官网：http://www.gcpowertools.com.cn/products/componentone_studio_asp.htm

下载：http://www.gcpowertools.com.cn/products/download.aspx?pid=54

社区：http://gcdn.gcpowertools.com.cn/showforum-137.html

视频：http://www.gcpowertools.com.cn/products/componentone_studio_asp_resources.htm

博客：http://blog.gcpowertools.com.cn/category/ComponentOne.aspx