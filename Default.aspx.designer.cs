﻿//------------------------------------------------------------------------------
// <自动生成>
//     此代码由工具生成。
//
//     对此文件的更改可能会导致不正确的行为，并且如果
//     重新生成代码，这些更改将会丢失。 
// </自动生成>
//------------------------------------------------------------------------------



public partial class _Default {
    
    /// <summary>
    /// PnlWhereMyMoney 控件。
    /// </summary>
    /// <remarks>
    /// 自动生成的字段。
    /// 若要进行修改，请将字段声明从设计器文件移到代码隐藏文件。
    /// </remarks>
    protected global::C1.Web.Wijmo.Controls.C1Expander.C1Expander PnlWhereMyMoney;
    
    /// <summary>
    /// ChrtSpending 控件。
    /// </summary>
    /// <remarks>
    /// 自动生成的字段。
    /// 若要进行修改，请将字段声明从设计器文件移到代码隐藏文件。
    /// </remarks>
    protected global::C1.Web.Wijmo.Controls.C1Chart.C1PieChart ChrtSpending;
    
    /// <summary>
    /// C1Expander1 控件。
    /// </summary>
    /// <remarks>
    /// 自动生成的字段。
    /// 若要进行修改，请将字段声明从设计器文件移到代码隐藏文件。
    /// </remarks>
    protected global::C1.Web.Wijmo.Controls.C1Expander.C1Expander C1Expander1;
    
    /// <summary>
    /// ChrtChecking 控件。
    /// </summary>
    /// <remarks>
    /// 自动生成的字段。
    /// 若要进行修改，请将字段声明从设计器文件移到代码隐藏文件。
    /// </remarks>
    protected global::C1.Web.Wijmo.Controls.C1Chart.C1BarChart ChrtChecking;
    
    /// <summary>
    /// PnlSavingsGolas 控件。
    /// </summary>
    /// <remarks>
    /// 自动生成的字段。
    /// 若要进行修改，请将字段声明从设计器文件移到代码隐藏文件。
    /// </remarks>
    protected global::C1.Web.Wijmo.Controls.C1Expander.C1Expander PnlSavingsGolas;
    
    /// <summary>
    /// C1PieChart1 控件。
    /// </summary>
    /// <remarks>
    /// 自动生成的字段。
    /// 若要进行修改，请将字段声明从设计器文件移到代码隐藏文件。
    /// </remarks>
    protected global::C1.Web.Wijmo.Controls.C1Chart.C1PieChart C1PieChart1;
    
    /// <summary>
    /// AcdChecking 控件。
    /// </summary>
    /// <remarks>
    /// 自动生成的字段。
    /// 若要进行修改，请将字段声明从设计器文件移到代码隐藏文件。
    /// </remarks>
    protected global::C1.Web.Wijmo.Controls.C1Accordion.C1Accordion AcdChecking;
    
    /// <summary>
    /// ApChecking 控件。
    /// </summary>
    /// <remarks>
    /// 自动生成的字段。
    /// 若要进行修改，请将字段声明从设计器文件移到代码隐藏文件。
    /// </remarks>
    protected global::C1.Web.Wijmo.Controls.C1Accordion.C1AccordionPane ApChecking;
    
    /// <summary>
    /// GrdChecking 控件。
    /// </summary>
    /// <remarks>
    /// 自动生成的字段。
    /// 若要进行修改，请将字段声明从设计器文件移到代码隐藏文件。
    /// </remarks>
    protected global::C1.Web.Wijmo.Controls.C1GridView.C1GridView GrdChecking;
    
    /// <summary>
    /// ObjectDataSource1 控件。
    /// </summary>
    /// <remarks>
    /// 自动生成的字段。
    /// 若要进行修改，请将字段声明从设计器文件移到代码隐藏文件。
    /// </remarks>
    protected global::System.Web.UI.WebControls.ObjectDataSource ObjectDataSource1;
    
    /// <summary>
    /// ApSavings 控件。
    /// </summary>
    /// <remarks>
    /// 自动生成的字段。
    /// 若要进行修改，请将字段声明从设计器文件移到代码隐藏文件。
    /// </remarks>
    protected global::C1.Web.Wijmo.Controls.C1Accordion.C1AccordionPane ApSavings;
    
    /// <summary>
    /// C1GridView1 控件。
    /// </summary>
    /// <remarks>
    /// 自动生成的字段。
    /// 若要进行修改，请将字段声明从设计器文件移到代码隐藏文件。
    /// </remarks>
    protected global::C1.Web.Wijmo.Controls.C1GridView.C1GridView C1GridView1;
    
    /// <summary>
    /// ObjectDataSource2 控件。
    /// </summary>
    /// <remarks>
    /// 自动生成的字段。
    /// 若要进行修改，请将字段声明从设计器文件移到代码隐藏文件。
    /// </remarks>
    protected global::System.Web.UI.WebControls.ObjectDataSource ObjectDataSource2;
    
    /// <summary>
    /// C1AccordionPane1 控件。
    /// </summary>
    /// <remarks>
    /// 自动生成的字段。
    /// 若要进行修改，请将字段声明从设计器文件移到代码隐藏文件。
    /// </remarks>
    protected global::C1.Web.Wijmo.Controls.C1Accordion.C1AccordionPane C1AccordionPane1;
    
    /// <summary>
    /// C1GridView2 控件。
    /// </summary>
    /// <remarks>
    /// 自动生成的字段。
    /// 若要进行修改，请将字段声明从设计器文件移到代码隐藏文件。
    /// </remarks>
    protected global::C1.Web.Wijmo.Controls.C1GridView.C1GridView C1GridView2;
    
    /// <summary>
    /// ObjectDataSource3 控件。
    /// </summary>
    /// <remarks>
    /// 自动生成的字段。
    /// 若要进行修改，请将字段声明从设计器文件移到代码隐藏文件。
    /// </remarks>
    protected global::System.Web.UI.WebControls.ObjectDataSource ObjectDataSource3;
    
    /// <summary>
    /// C1AccordionPane2 控件。
    /// </summary>
    /// <remarks>
    /// 自动生成的字段。
    /// 若要进行修改，请将字段声明从设计器文件移到代码隐藏文件。
    /// </remarks>
    protected global::C1.Web.Wijmo.Controls.C1Accordion.C1AccordionPane C1AccordionPane2;
    
    /// <summary>
    /// C1GridView3 控件。
    /// </summary>
    /// <remarks>
    /// 自动生成的字段。
    /// 若要进行修改，请将字段声明从设计器文件移到代码隐藏文件。
    /// </remarks>
    protected global::C1.Web.Wijmo.Controls.C1GridView.C1GridView C1GridView3;
    
    /// <summary>
    /// ObjectDataSource4 控件。
    /// </summary>
    /// <remarks>
    /// 自动生成的字段。
    /// 若要进行修改，请将字段声明从设计器文件移到代码隐藏文件。
    /// </remarks>
    protected global::System.Web.UI.WebControls.ObjectDataSource ObjectDataSource4;
}
